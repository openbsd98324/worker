// generated by Fast Light User Interface Designer (fluid) version 1.0304

#ifndef flworker_h
#define flworker_h
#include <FL/Fl.H>
int fexist( const char *a_option );
void load_content( int fooz );
#include <FL/Fl_Double_Window.H>
extern Fl_Double_Window *win1;
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_File_Browser.H>
extern Fl_File_Browser *form1_panel1;
extern Fl_File_Browser *form1_panel2;
#include <FL/Fl_Output.H>
extern Fl_Output *form1_currentfile;
#include <FL/Fl_Clock.H>
#include <FL/Fl_Input.H>
extern Fl_Input *form1_panel1_path;
extern Fl_Input *form1_panel2_path;
extern Fl_Input *form1_panel1_filter;
extern Fl_Input *form1_panel2_filter;
extern Fl_Double_Window *form1_periodic_information;
#include <FL/Fl_Browser.H>
extern Fl_Browser *form_content_browser1;
extern Fl_Double_Window *win3;
extern Fl_Input *execute_inputbox_command;
extern Fl_Double_Window *win4;
extern Fl_Input *inputbox_rename_filesource;
extern Fl_Double_Window *form_inputbox;
extern Fl_Input *modal_inputbox_userstring;
Fl_Double_Window* make_window();
void panel_show_list();
int main( int argc, char *argv[]);
void redraw_window();
void form1_pan1_click();
void form1_pan2_click();
#endif
